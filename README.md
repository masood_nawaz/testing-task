**Answers:**

**Task #1: Writing Test Scenarios**
1.	Verify that online check-in form contains reference number, last name and relevant information for check-in.
2.	Verify that confirmation email, SMS is working proper.
3.	Verify that Enter/Tab key works as a substitute for the Submit button.
4.	Verify that all the fields such as last name, reference number have a valid placeholder.
5.	Verify that all the required/mandatory fields are marked with * against the field.
6.	Verify that clicking on submit button after entering all the mandatory fields, submits the data to the server.
7.	Verify that system generates a validation message when clicking on submit button without filling all the mandatory fields.
8.	Verify that entering blank spaces on mandatory fields lead to validation error.
9.	Verify that the character limit in all the fields (mainly last name and booking reference) based on business requirement.
10.	Verify that case sensitivity of last name and reference number.
11.	Verify that the character limit in all the fields (mainly in reference number and passport number) based on business requirement.
12.	Verify that the date of birth field should not allow the dates greater than current date.
13.	Verify that user is able to check-in with valid credentials
14.	Verify that user is not able to check-in with invalid last name and invalid reference number.
15.	Verify that clicking on browser back button after successful check-in should not take user to log out mode.
16.	Verify that clicking on browser back button after successful logout should not take user to logged in mode.
17.	Verify that there is a limit on the total number of unsuccessful check-in attempts.
18.	Verify the Capability to take/upload a picture of the passport that's a mandatory field for Non-German nationalities. 
19.	To verify whether check-in date and check Out date are being displayed in check-in page according to the dates selected in book hotel page.
20.	To verify whether the data displayed is same as the selected data in book hotel page.
21.	To Verify Title of every page reflects what the page objective is, for example title of check-in hotel page should have “check-in Hotel”.
22.	Verify that bilingual is working proper (for German and English).
23.	Verify that all the details of newly generated order number in booked itinerary page are correct and match with data during booking.
24.	To verify whether the booked itinerary details are not editable.
25.	Verify that the pages are correctly displayed when opened in mobile.
26.	Verify that SQL injections cannot be used.
27.	Verify that page load time is less than 200 milliseconds. 


**Task #2: Finding Defects**
1.	Bilingual is not working on both pages.
2.	Submit button on check-in page is not proper visible.
3.	Postal code has an unlimited character limit.
4.	Type and max size of passport picture is missing.
5.	The date of birth field is allowing the dates greater than current date.
6.	Passport id has an unlimited character limit and takes any characters.
7.	Booking reference number and last name should not be visible in URL.




**Task #3: Defect Reporting**


**Bug Name:** Passport id has an unlimited character limit and takes any characters.

**Bug ID:** (It will be automatically created by the BUG Tracking tool once you save this bug)

**Build Number:** Version number 2.0.1

**Severity:** HIGH (High/Medium/Low) or 1

**Priority:** HIGH (High/Medium/Low) or 1

**Assigned to:** Developer-X

**Reported By:** Your Name

**Reported On:** Date

**Reason:** Defect

**Status:** New/Open/Active (Depends on the Tool you are using)

**Environment:** Windows 2010/SQL Server

**URL:** https://limehome-qa-task.herokuapp.com/registrationForm/qwerty12345/ali

**Screenshot:** www.limehome.com/screenshot12

**Description:**
Passport id has an unlimited character limit and takes any characters, including special characters like @,# etc. There is high chance user maybe enter wrong passport number which is very important for booking.

**Steps To Reproduce:**
 1) check-in into the application by using correct details for example (last name= ali , booking reference number=qwerty12345).
 2) Check all the given information.
 3) Filled the missing user information with valid values for example ( date of birth= 15/04/1995, Type of stay= Business).
 4) Select any Non-German nationality for example (British GB)
 5) Enter invalid the passport number for example (zxcvbgty12345678945612312478) 
 6) And also see the attached screenshot of the error page.
 
**Expected Result:** Passport number is not in correct format.

**Actual Results:** /

**Notes:** /

****
